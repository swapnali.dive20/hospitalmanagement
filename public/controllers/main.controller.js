var app = angular.module('myfirstmean',[]);

app.controller('addcontrolle',function($scope,$http){

  var displayDoctor = function(){
    $http.get('/getDoctor').then(function(response){
      console.log("get doctor response : ",response);
      $scope.doctorDetails = response.data;      
    });
  };

  displayDoctor();

  var displayPatient = function(){
    $http.get('/getPatient').then(function(response){
      $scope.patientDetails = response.data;      
    });
  };

  displayPatient();
  
  $scope.addPatient = function(){
    $http.post("/setPatient",$scope.patient).then(function(response){
      console.log(response);      
    })
  };  
  
  $scope.addDoctor = function(){
    $http.post("/setDoctor",$scope.doctor).then(function(response){
      console.log(response);      
    })
  };

});
