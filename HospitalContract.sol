pragma solidity ^0.5.0;

contract Hospital {
    
    struct Patient
    {       
        uint256 patientId;      
        string patientName;                 
        string patientEmail;
        uint256  patientContact;
        string patientResidence;
        address patientAddress;
    }
    
    struct Doctor
    {
        uint256 doctorId;       
        string doctorName;                  
        string doctorEmail;
        uint256  doctorContact;
        string doctorResidence;   
    }
    
    
    mapping (address => Patient) patients;
    address[] public patientList;
    
    mapping (address => Doctor) doctors;
    address[] public doctorList;
    
    //address[] public doctorWisePatient;
    mapping (address => address[]) doctorWisePatient;
    mapping (address => address) patientWiseDoctor;
    
    
    function setPatient(uint _pId, string memory  _pName, string memory  _pEmail, uint _pContact, string memory _pResidence) public {
        Patient storage patient = patients[msg.sender];

        patient.patientId = _pId;
        patient.patientName = _pName;
        patient.patientEmail = _pEmail;
        patient.patientContact = _pContact;
        patient.patientResidence = _pResidence;
        patient.patientAddress = msg.sender;

        
        patientList.push(msg.sender) -1;
    }
    
    function getPatient(address _pAddress) view public returns (uint, string memory, string memory,uint, string memory) {
        if(_pAddress == msg.sender || patientWiseDoctor[_pAddress] == msg.sender)
        {
            return (patients[_pAddress].patientId,patients[_pAddress].patientName,patients[_pAddress].patientEmail,patients[_pAddress].patientContact,patients[_pAddress].patientResidence);
        }
    }

    function consultWithDoctor(address _dAddress) public
    {
            doctorWisePatient[_dAddress].push(msg.sender);
            patientWiseDoctor[msg.sender] = _dAddress;
            
    }
    
    function patientToTreat() view public returns(address[] memory)
    {
            return (doctorWisePatient[msg.sender]);
    }
    
    function setDoctor(uint _dId, string memory  _dName, string memory  _dEmail, uint _dContact, string memory _dResidence) public {
        Doctor storage doctor = doctors[msg.sender];

        doctor.doctorId = _dId;
        doctor.doctorName = _dName;
        doctor.doctorEmail = _dEmail;
        doctor.doctorContact = _dContact;
        doctor.doctorResidence = _dResidence;

        
        doctorList.push(msg.sender) -1;
    }
    
    function getDoctor() view public returns (uint, string memory, string memory,uint, string memory) {
        address _dAddress = msg.sender;
        return (doctors[_dAddress].doctorId,doctors[_dAddress].doctorName,doctors[_dAddress].doctorEmail,doctors[_dAddress].doctorContact,doctors[_dAddress].doctorResidence);
    }
    
}