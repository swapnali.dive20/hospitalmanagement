# Hospital Admissions and Assignments of Patients using Ethereum Smart Contract

# Run Application

```sh
git clone https://gitlab.com/swapnali.dive20/hospitalmanagement.git
```

```sh
cd hospitalmanagement
```

```sh
npm install
```

```sh
npm run start
```

* Server will start in localhost:3000 port
