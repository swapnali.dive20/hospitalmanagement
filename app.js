var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var index = require('./routes/index');
const web3 = require('web3');
const Tx = require('ethereumjs-tx');
var HDWalletProvider = require("truffle-hdwallet-provider");

var app = express();

//Infura HttpProvider Endpoint

const walletMnemonic = 'enact umbrella gas shine shove lumber wet excuse quote shove scan rival'; // Your mnemonic
const walletAPIUrl = 'https://rinkeby.infura.io/8U0AE4DUGSh8lVO3zmma'; // Your Infura URL

const provider = new HDWalletProvider(
    walletMnemonic,
    walletAPIUrl
);

const web3js = new web3(provider);

//web3js = new web3(new web3.providers.HttpProvider("https://rinkeby.infura.io/8U0AE4DUGSh8lVO3zmma"));

if(web3js){
  console.log('Infura Connected');
}else{
  console.log('Infura NOT Connected');
}

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);

var contractABIHospital = [
	{
		"constant": false,
		"inputs": [
			{
				"name": "_dAddress",
				"type": "address"
			}
		],
		"name": "consultWithDoctor",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_dId",
				"type": "uint256"
			},
			{
				"name": "_dName",
				"type": "string"
			},
			{
				"name": "_dEmail",
				"type": "string"
			},
			{
				"name": "_dContact",
				"type": "uint256"
			},
			{
				"name": "_dResidence",
				"type": "string"
			}
		],
		"name": "setDoctor",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_pId",
				"type": "uint256"
			},
			{
				"name": "_pName",
				"type": "string"
			},
			{
				"name": "_pEmail",
				"type": "string"
			},
			{
				"name": "_pContact",
				"type": "uint256"
			},
			{
				"name": "_pResidence",
				"type": "string"
			}
		],
		"name": "setPatient",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"name": "doctorList",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_dAddress",
				"type": "address"
			}
		],
		"name": "getDoctor",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_pAddress",
				"type": "address"
			}
		],
		"name": "getPatient",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"name": "patientList",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "patientToTreat",
		"outputs": [
			{
				"name": "",
				"type": "address[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
];

var contractAddressHospital = "0x3608afa7941470b78044a5eedade93718d8dfb42";

//creating contract object
var contractHospital = new web3js.eth.Contract(contractABIHospital,contractAddressHospital);

var currentAccount = '';

web3js.eth.getAccounts(function(err, accounts){
    if (err != null) {
      console.log(err)
    }
    else if (accounts.length === 0) {
      console.log('MetaMask is locked')
    }
    else{
      currentAccount = accounts[0];      
      console.log("current address here: ",currentAccount);   
    }
});

app.post('/setPatient', (req, res) => {
  console.log("Creating new patient...");
  var id = req.body.id;
  var name = req.body.name;
  var email = req.body.email;
  var contact = req.body.contact;
  var residence = req.body.residence;
  contractHospital.methods.setPatient(id,name,email,contact,residence).send({from:currentAccount, to:contractAddressHospital},(err, result) => { 
    if(!err)
    {
      console.log("patient created : ",result) 
    }
    else{
      console.log("error in method set string") 
    }    
  })
});

app.get('/getPatient', (req, res) => {
  contractHospital.methods.getPatient(currentAccount).call((err, result) => { 
    if(!err)
    {
      //console.log(result);     
      var patientObj = {
        'id' :result[0],
        'name' :result[1],
        'email' :result[2],
        'contact' :result[3],
        'residence' :result[4],
      }
      res.json(patientObj);
      console.log(patientObj);
    }
    else{
      console.log("error while fetching patient Information") 
    }    
  })
});

app.post('/setDoctor', (req, res) => {
  console.log("Creating new doctor...");
  var id = req.body.id;
  var name = req.body.name;
  var email = req.body.email;
  var contact = req.body.contact;
  var residence = req.body.residence;
  contractHospital.methods.setDoctor(id,name,email,contact,residence).send({from:currentAccount, to:contractAddressHospital},(err, result) => { 
    if(!err)
    {
      console.log("Doctor created : ",result) 
    }
    else{
      console.log("error in method set string") 
    }    
  })
});

app.get('/getDoctor', (req, res) => {
  contractHospital.methods.getDoctor(currentAccount).call((err, result) => { 
    if(!err)
    {
      //console.log(result);     
      var doctorObj = {
        'id' :result[0],
        'name' :result[1],
        'email' :result[2],
        'contact' :result[3],
        'residence' :result[4],
      }
      res.json(doctorObj);
      console.log(doctorObj);
    }
    else{
      console.log("error while fetching doctor Information") 
    }    
  })
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
	//res.render('error');
	res.json({
		message: err.message,
		error: err
	});
});

module.exports = app;
